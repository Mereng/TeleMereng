package main

import (
	"io/ioutil"
	"path"
	"regexp"
	"os"
)

type sshError struct {
	error
	Message string
}

func (err sshError) Error() string {
	return err.Message
}

func readSSHs(home string) ([][]string, error) {
	data, err := ioutil.ReadFile(path.Join(home, "authorized_keys"))

	if err != nil {
		return nil, err
	}

	r, _ := regexp.Compile(REGEX_VALID_SSH)

	sshs := r.FindAllStringSubmatch(string(data), -1)

	return sshs, nil
}

func addSSH(sshKey, home string) error {
	r, _ := regexp.Compile(REGEX_VALID_SSH)
	sshValid := r.FindAllString(sshKey, -1)
	if sshValid == nil {
		return sshError{Message: "Не валидный ssh-ключ"}
	}

	sshs, errReadSSH := readSSHs(home)

	if errReadSSH != nil {
		return errReadSSH
	}

	for _, ssh := range sshs {
		if ssh[0] == sshKey {
			return sshError{Message: "Такой ключ уже есть"}
		}
	}

	f, err := os.OpenFile(path.Join(home, "authorized_keys"), os.O_APPEND | os.O_WRONLY, 0664)
	defer f.Close()
	if err != nil {
		return err
	}

	f.WriteString("\n" + sshValid[0])
	f.Close()

	return nil
}