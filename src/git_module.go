package main

import (
	"github.com/Syfaro/telegram-bot-api"
	"strings"
	"os/exec"
	"os"
	"syscall"
	"log"
	"fmt"
	"path"
	"io/ioutil"
	"bytes"
	"regexp"
)

func generateGitLink(repo string) string {
	return fmt.Sprintf("%s:%s", system.GitSshLink, path.Join(config.DirRepos, repo))
}

func gitManage(update tgbotapi.Update) {
	arguments := strings.Split(update.Message.CommandArguments(), " ")

	if len(arguments) < 1 {
		return
	}

	switch arguments[0] {
	case "init":
		gitInit(arguments[1:], update)
	case "remove":
		gitRemove(arguments[1:], update)
	case "ls":
		gitLs(update)
	case "ssh":
		gitSsh(arguments[1:], update)
	}
}

func gitInit(args []string, update tgbotapi.Update)  {
	os.Chdir(system.PathRepos);
	if len(args[0]) > 0 {
		repo := args[0] + ".git";
		if _, err := os.Stat(repo); !os.IsNotExist(err) {
			system.Bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Такой репозиторий уже существует"))
			return
		}

		os.Mkdir(repo, 0777);
		os.Chown(repo, int(system.GitUid), int(system.GitGid))
		os.Chdir(repo)
		cmd := exec.Command("git", "--bare", "init")
		cmd.SysProcAttr = &syscall.SysProcAttr{}
		cmd.SysProcAttr.Credential = &syscall.Credential{Uid: system.GitUid, Gid: system.GitGid}
		_, err := cmd.Output();

		if err != nil {
			log.Println(err.Error())
			sendError(update)
			return
		}
		system.Bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Ваша ссылка: " + generateGitLink(repo)))
	}
}

func gitRemove(args []string, update tgbotapi.Update)  {
	if len(args) <= 0 {
		return
	}
	repo := path.Join(system.PathRepos, args[0] + ".git")
	if _, err := os.Stat(repo); os.IsNotExist(err) {
		system.Bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Такого репозитория нет"))
		return
	}

	err := os.RemoveAll(repo)

	if err != nil {
		log.Println(err.Error())
		sendError(update)
		return
	}

	system.Bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Репозиторий удален успешно"))
}

func gitLs(update tgbotapi.Update)  {
	files, err := ioutil.ReadDir(system.PathRepos)

	if err != nil {
		sendError(update)
		return
	}

	var bufferList bytes.Buffer
	bufferList.WriteString("Список репозиториев:\n")
	isReposExist := false
	r, _ := regexp.Compile(`(.*).git`)
	for _, f := range files {
		name := r.FindStringSubmatch(f.Name())
		if name != nil {
			bufferList.WriteString(fmt.Sprintf("\u2014 %s (%s)\n", name[1], generateGitLink(name[1])))
			isReposExist = true
		}
	}

	if isReposExist {
		system.Bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, bufferList.String()))
	} else {
		system.Bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Список репозиториев пуст."))
	}
}

func gitSsh(args []string, update tgbotapi.Update) {
	if len(args) < 1 {
		return
	}
	switch args[0] {
	case "add":
		gitSshAdd(update, false)
	}
}

func gitSshAdd(update tgbotapi.Update, add bool) {
	if !add {
		system.Bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Вставьте публичный SSH-ключ"))
		system.PrevCommand = prcmdGitSshAdd
		return
	}

	err := addSSH(update.Message.Text, system.GitSshHome)

	if err != nil {
		if e, ok := err.(sshError); ok {
			system.Bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, e.Error()))
			return
		}
		sendError(update)
		log.Println(err.Error())
	}

	system.Bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Ключ успешно добавлен"))
}