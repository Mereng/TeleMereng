package main

import (
	"github.com/Syfaro/telegram-bot-api"
	"io/ioutil"
	"path/filepath"
	"os"
	"log"
	"path"
	"encoding/json"
	"os/user"
	"strconv"
	"fmt"
)

const (
	prcmdGitSshAdd = iota + 1
)

const (
	REGEX_VALID_SSH = `ssh-rsa .* (.*)`
)

var config struct {
	Token 		string `json:"token"`
	DirRepos 	string `json:"dir_repos"`
	GitUser 	string `json:"git_user"`
	Host 		string `json:"host"`
}

var system struct{
	Bot         *tgbotapi.BotAPI
	GitUid      uint32
	GitGid      uint32
	GitSshLink  string
	GitSshHome  string
	PathRepos   string
	PrevCommand int
}

func main() {
	readConfig();
	gitUser, errUser := user.Lookup(config.GitUser)

	if errUser != nil {
		log.Panic(errUser)
	}

	uid, _ := strconv.Atoi(gitUser.Uid)
	gid, _ := strconv.Atoi(gitUser.Gid)

	system.GitUid = uint32(uid)
	system.GitGid = uint32(gid)

	system.GitSshLink = fmt.Sprintf("%s@%s", config.GitUser, config.Host)
	system.GitSshHome = path.Join("/home/", config.GitUser, ".ssh")
	system.PathRepos = path.Join("/home/", config.GitUser, config.DirRepos);

	var errBot error
	system.Bot, errBot = tgbotapi.NewBotAPI(config.Token)
	if errBot != nil {
		log.Panic(errBot)
	}

	log.Printf("Authorazied on account %s", system.Bot.Self.UserName);

	ucfg := tgbotapi.NewUpdate(0);
	ucfg.Timeout = 60;
	updates, errBot := system.Bot.GetUpdatesChan(ucfg);

	for update := range updates {
		if update.Message == nil {
			continue
		}

		switch update.Message.Command() {
		case "git":
			gitManage(update)
		default:
			checkPrevCmd(update)
		}
	}
}

func readConfig() {
	configFile, errPath := filepath.Abs(filepath.Dir(os.Args[0]))
	if (errPath != nil) {
		log.Panic(errPath);
	}
	data, err := ioutil.ReadFile(path.Join(configFile, "config.json"))

	if (err != nil) {
		log.Panic(err)
	}

	json.Unmarshal(data, &config)
}

func sendError(update tgbotapi.Update) {
	system.Bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "ОШИБКА!!"))
}

func checkPrevCmd(update tgbotapi.Update) {
	switch system.PrevCommand {
	case prcmdGitSshAdd:
		gitSshAdd(update, true)
	}
	system.PrevCommand = 0;
}